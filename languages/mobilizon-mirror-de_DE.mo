��    4      �  G   \      x     y     |     �  
   �  
   �     �     �     �  	   �  �   �  	   �     �  '   �     �     �  @   �     6     >     K     `  D   {      �  
   �  t  �     a     p     �     �     �     �  O   �     	     	  	   0	  
   :	     E	  
   K	     V	  	   g	     q	     �	     �	  (   �	  7   �	  	   �	     	
     
     %
  2   -
     `
     e
  n  h
     �     �     �            "   +     N     V     o  �        n  '   �  /   �     �     �  \         ]     j     v     �  a   �  )        6  v  C     �     �     �     �     �       O   (     x  "   �     �     �     �     �     �     �     �            &   $  A   K     �     �  
   �     �  J   �                       2      *                           .   "      3                        !           ,             (       -                    
   1            %      )           +   #   0   &   4            $                        /             	      '       %s %s Attributes Account Information Add New %s Add new %s Add or remove %s All %s Archive Style As Header By default this plugin uses a prefixed post type called mobilizon_event, but the slug events, may conflict and cause problems. This may be addressed in the future. Card View Choose from the most used %s Currently there are no events scheduled Date and Time Display Options Does the plugin conflict with other Event-Plugins or Post-Types? Edit %s Every minute Federated Group Name Frequently Asked Questions How can I add my synced events to the navigation menu of my website? How often are the events synced? In Sidebar In your admin navigation menu go to "Appearance"->"Menu". Then make sure that the "Screen Options" (accessable on the top right) "Mobilizon Events" are marked as visible. Then under "Add menu items" you can select "Mobilizon Events"->"View All"->"Mobilizon Event List". Then you can choose the Navigation Label (the name as it appears for your sites visitors) by yourself. Insert into %s Mobilizon Instance (URL) New %s New %s Name No %s found No %s found in trash Not the Display Name! &#10;The slug behind the @ in the url of your group page! Organized by Originally posted on  Parent %s Parent %s: Place Popular %s Save all changes Search %s Separate %s with commas Settings Simple List Single Event: Position of Featured Image The events are synced every two minutes from Mobilizon. Update %s Uploaded to this %s View %s Website Where do I find the mirrored events on my website? from to Project-Id-Version: 
PO-Revision-Date: 2021-08-12 12:12+0200
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;esc_attr_e;esc_html_e
X-Poedit-SearchPath-0: .
 %s %s-Eigenschaften Account-Informationen Füge neues %s hinzu Füge neues %s hinzu Löschen oder Hinzufügen eines %s Alle %s Stil der Eventübersicht Als Banner oben Standardmäßig verwendet dieses Plugin einen geprefixten Post-Typ namens mobilizon_event, aber der Slug events, kann in mit bestehenden in Konflikt geraten und Probleme verursachen. Dies wird möglicherweise in Zukunft verbessert werden. Kärtchen mit Bild Wähle eines der am besten genutzten %s Momentan sind keine Veranstaltungen eingetragen Datum und Uhrzeit Darstellungsoptionen Kann das Plugin Konflikte mit anderen Event-Plugins oder einem Custom-Post-Type verursachen? Bearbeite %s Jede Minute Föderierter Gruppenname Frequently Asked Questions Wie kann ich die synchronisierten Events zum Navigationsmenü meiner WordPress Seite hinzufügen? Wie oft werden die Events synchronisiert? An der Seite Gehen Sie in Ihrem Admin-Navigationsmenü auf "Design"->"Menüs". Vergewissern Sie sich dann, dass unter "Ansicht anpassen" (erreichbar oben rechts) "Mobilizon Events" als sichtbar markiert ist. Unter "Menüeinträge hinzufügen" können Sie dann "Mobilizon Events"->"Alle anzeigen"->"Mobilizon Event List" auswählen. Dann können Sie  den Angezeigter Namen selbst wählen. In %s einfügen Mobilizon Instance (URL) Neues %s Name für neues %s Keine %s gefunden Keine %s im Papierkorb Nicht der Anzeigename! &#10;Der Teil hinter dem @ in dem Link zu deiner Gruppe! Organisator Ursprünglich veröffentlicht auf  Übergeordnete %s Übergeordnetes %s: Ort Beliebte %s Übernehmen Suche %s Trenne %s mit Kommas Einstellungen Einfache Liste Eventansicht: Position des Titelbildes Die Events werden von Mobilizon alle zwei Minuten synchronisiert. %s aktualisieren Zu diesem %s hochladen %s ansehen Website Wo finde ich die gespiegelten Mobilizon-Events auf meiner WordPress-Seite? von bis 
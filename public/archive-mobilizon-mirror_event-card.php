<?php
/* 
Template Name: Mobilizon_Mirror Event Archive Cards
*/
get_header(); ?>
 
<div id="primary" class="site-content">
<div id="event-card-list" role="main">

<?php 

// Query concerts.
$query_args = array(
    'posts_per_page' => -1,
    'post_type'      => 'mobilizon_event',
    'order'          => 'ASC',
    'orderby'        => 'meta_value',
    'meta_key'       => 'beginsOn',
    'meta_type'      => 'DATETIME'
);

$the_query = new WP_Query($query_args);

if ( $the_query->have_posts() ) : ?>
<div class="event-columns">
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    <div class="event-card-container">
        <a class="event-card" href="<?php the_permalink()?>">
            <figure class="event-card-image">
                <?php $post_thumbnail = get_the_post_thumbnail() ;
                    if( $post_thumbnail == '' ) { ?>
                        <div class="is-16by9"> </div>
                    <?php  }  else {
                        echo $post_thumbnail;
                }?>
                <div class="tag-container">
                    <?php foreach (wp_get_post_tags( get_the_ID(), array('number' => 3) ) as $tag) {?>
                        <span  href="/tag"><?php echo $tag->name ?></span>
                        <?php }?>
                </div>
            </figure>
            <div class="card-content">
                <div class="media-left">
                    <time class="datetime-container">
                        <div class="datetime-container-header"></div>
                        <div class="datetime-container-content">
                            <?php $beginsOn = get_post_meta( get_the_ID(), 'beginsOn' )[0]; ?>
                            <span class="day"> <?php  echo date_i18n( 'd', strtotime($beginsOn));?></span>
                            <span class="month"><?php  echo date_i18n( 'M', strtotime($beginsOn));?></span>
                        </div>
                    </time>
                </div>
                <h4 class="event-title"><?php the_title(); ?></h4>
            </div>
        </a>
    </div>
    <?php endwhile; // end of the loop. ?>
</div>
 

<?php else : ?>
	<p><?php esc_html_e( 'Currently there are no events scheduled' ); ?></p>
<?php endif; ?>


</div><!-- #event-card-list -->
</div><!-- #primary -->
 

<?php get_footer(); ?>
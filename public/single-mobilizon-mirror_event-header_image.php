<?php
/**
 * The template for displaying a single event
 *
 *
 * @package Mobilizon_Mirror
 * @subpackage Mobilizon_Mirror
 * @since 1.0.0
 */

get_header();
?>

<div id="event">
    <?php the_post_thumbnail( );?>
    <div class="intro-wrapper">
        <?php if ( has_post_thumbnail() ) { ?>
            <div class="date-calendar-icon-wrapper">
                <time class="datetime-container">
                    <div class="datetime-container-header"></div>
                    <div class="datetime-container-content">
                        <?php $beginsOn = get_post_meta( get_the_ID(), 'beginsOn' )[0]; ?>
                        <span class="day"> <?php  echo date_i18n( 'd', strtotime($beginsOn));?></span>
                        <span class="month"><?php  echo date_i18n( 'M', strtotime($beginsOn));?></span>
                    </div>
                </time>
            </div>
        <?php } ?>
        <h2 class="event-title">
            <?php the_title(); ?>
        </h2>
        <div class="event-tag-list">
            <?php foreach (wp_get_post_tags(get_the_ID()) as $tag) { ?>
            <a href="<?php echo get_term_link( $tag, $taxonomy = 'tag' );?>"><button class="wp-block-button"><?php echo $tag->name ?></button></a>
            <?php } ?>
        </div>
    </div>
    <div class="event-description-wrapper"> 
        <aside class="event-metadata">
            <?php $metadata = get_post_meta( get_the_ID() ); ?>
            <h4> <?php esc_html_e( 'Place', 'mobilizon-mirror' ) ?> </h4>
            <div class="eventMetaDataBlock">
            <?php echo join("<br>",array($metadata['place'][0], 
                                         $metadata['street'][0],
                                         $metadata['postalCode'][0] . ' ' . $metadata['city'][0]
                                         )); ?>
            </div>
            <h4> <?php esc_html_e( 'Date and Time', 'mobilizon-mirror' )?> </h4>
            <div class="eventMetaDataBlock">
                <?php   $date = date_i18n( 'D, d. M. Y', strtotime($metadata['beginsOn'][0]), true);
                        $start = date_i18n(' G:i ', strtotime($metadata['beginsOn'][0]),false);
                        $end = date_i18n('G:i', strtotime($metadata['endsOn'][0]));
                        echo ($date . ' ' . __('from','mobilizon-mirror') . $start . ' ' . __('to','mobilizon-mirror') . ' ' . $end ); ?>
            </div>
            <h4> <?php esc_html_e( 'Organized by', 'mobilizon-mirror' )?> </h4>
            <div class="eventMetaDataBlock">
                <a href="<?php echo $metadata['organizerURL'][0]; ?>" target="blank"><?php echo $metadata['organizerName'][0] ;?></a>
            </div>	
            <h4> <?php esc_html_e( 'Website', 'mobilizon-mirror' )?> </h4>
            <div class="eventMetaDataBlock">
                <a href="<?php echo $metadata['onlineAddress'][0];?>" target="blank"><?php echo(parse_url($metadata['onlineAddress'][0], PHP_URL_HOST));?></a>
            </div>	
        </aside>
        <article class="event-description">
            <?php the_content(); ?>
          
        </article>
        
    </div>
    <hr>
    <footer class="event-url">
    <?php echo esc_html_e('Originally posted on ', 'mobilizon-mirror') ?>
       <a href="<?php echo $metadata['url'][0]; ?>"><?php $url_parts = parse_url($metadata['url'][0] ); echo $url_parts['scheme'] . '://' . $url_parts['host'] ?></a>
    </footer>




</main>
<?php get_footer(); ?>
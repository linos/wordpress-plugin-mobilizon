<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://graz.social/@linos
 * @since      1.0.0
 *
 * @package    Mobilizon_Mirror
 * @subpackage Mobilizon_Mirror/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Mobilizon_Mirror
 * @subpackage Mobilizon_Mirror/public
 * @author     André Menrath <andre.menrath@posteo.de>
 */
class Mobilizon_Mirror_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Mobilizon_Mirror_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Mobilizon_Mirror_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/mobilizon-mirror-public.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Mobilizon_Mirror_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Mobilizon_Mirror_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/mobilizon-mirror-public.js', array( 'jquery' ), $this->version, false );

	}
	
	/**
	 *  Filter and selectively load the mobilizon template inside plugin.
	 *
	 * @since    1.0.0
	 */
	public function mobilizon_mirror_event_template($template) {
		if ( is_post_type_archive('mobilizon_event') ) {
			$theme_files = array('archive-mobilizon-mirror_event.php', 'mobilizon-mirror/archive-mobilizon-mirror_event.php');
			$exists_in_theme = locate_template($theme_files, false);
			if ( $exists_in_theme != '' ) {
				return $exists_in_theme;
			} else {
				if (get_option($this->plugin_name)['event_archive_view'] == 'list') {
					return plugin_dir_path(__FILE__) . 'archive-mobilizon-mirror_event-list.php';
				} else {
					return plugin_dir_path(__FILE__) . 'archive-mobilizon-mirror_event-card.php';
				}
			}
		} else if ( get_post_type() == 'mobilizon_event' ) {
			$theme_files = array('single-mobilizon-mirror_event.php', 'mobilizon-mirror/single-mobilizon-mirror_event.php');
			$exists_in_theme = locate_template($theme_files, false);
			if ( $exists_in_theme != '' ) {
				return $exists_in_theme;
			} else {
				if (get_option($this->plugin_name)['event_single_view'] == 'top') {
					return plugin_dir_path(__FILE__) . 'single-mobilizon-mirror_event-header_image.php';
				} else {
					return plugin_dir_path(__FILE__) . 'single-mobilizon-mirror_event-side_image.php';
				}
			}
		}
		return $template;
	}


	public function mobilizon_mirror_include_mobilizon_event_in_search_results( $query ) {
		if ( $query->is_main_query() && $query->is_search() && ! is_admin() ) {
			$query->set( 'post_type', array( 'post', 'page', 'mobilizon_event') );
		}
	}


}

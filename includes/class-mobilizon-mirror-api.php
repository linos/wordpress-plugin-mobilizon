<?php

/**
 * Wrapper for fetching data from mobilizon instances
 *
 * @link       https://graz.social/@linos
 * @since      1.0.0
 *
 * @package    Mobilizon_Mirror
 * @subpackage Mobilizon_Mirror/includes
 */
class Mobilizon_Mirror_API {
    /**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;


	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 */
	public function __construct( $plugin_name ) {
		$this->plugin_name = $plugin_name;
	}


    /**
	 * Adds https:// protokoll if no url scheme is present
	 *
	 * @since    1.0.0
	 * @access   private
	 * @param      string    $url    The URL which is processed
	 */
    private function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "https://" . $url;
        }
        return $url;
    }


    /**
	 * Adds https:// protokoll if no url scheme is present
	 *
	 * @since    1.0.0
	 * @access   private
	 * @param    string     $baseURL    the domain/hostname/url of the mobilizon instance
     * @param    string     $query      the graphql query string see https://framagit.org/framasoft/mobilizon/-/blob/master/schema.graphql
     */
    private function mobilizon_do_query($baseURL, $query) {
        // Get API-endpoint from Instance URL
        $baseURL = rtrim($baseURL, '/');
        $url_array = array($baseURL, "api");
        $endpoint = implode('/', $url_array);
        $endpoint = $this->addhttp($endpoint);

        // Define default GraphQL headers
        $headers = ['Content-Type: application/json', 'User-Agent: Minimal GraphQL client'];
        $body = array ('query' => $query);
        $args = array(
            'body'        => $body,
            'headers' 	  => $headers,
        );

        // Send HTTP-Query and return the response
        return(wp_remote_post($endpoint, $args));
    }

    /**
	 * Inserts or updates a new post of post type mobilizon_events
	 *
	 * @since    1.0.0
	 * @access   private
	 * @param    array     $event    array containing all the wordpress default and metadata fields for a post
     * @return    mixed    $post_id  Is the post_id as int on success, else WP_Error or 0
     */
    private function insert_or_update_mobilizon_event_as_post($event) {
        require_once(ABSPATH . 'wp-admin/includes/media.php');
        require_once(ABSPATH . 'wp-admin/includes/file.php');
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $my_post = array(
            'post_title' => $event['title'],
            'post_date' => $event['updatedAt'],
            'post_content' => $event['description'],
            'post_status' => 'publish',
            'post_type' => 'mobilizon_event',
            'post_status' => ($event['visibility'] === "PUBLIC") ? "publish" : "private",
           );
           
        if (isset($event["post_id"])) {
            // It's an updated Event from Mobilizon
            $my_post['ID'] = $event["post_id"];
            $post_id = wp_update_post($my_post);
        } else {
            // It's a new Event from Mobilizon
            $post_id = wp_insert_post($my_post);
        }

        // Abort if inserting or updating the post didn't work
        if ( $post_id == 0 || is_wp_error($post_id) ) {
            return $post_id;
        }

        wp_set_post_tags( $post_id, array_column($event['tags'], 'title'));
        //var_dump (array_column($event['tags'], 'title'));
        //die();
        add_post_meta($post_id, 'beginsOn',        $event['beginsOn']);
        add_post_meta($post_id, 'endsOn',          $event['endsOn']);
        add_post_meta($post_id, 'onlineAddress',   $event['onlineAddress']);
        add_post_meta($post_id, 'organizerName',   (isset($event['organizerActor']['name'])) ? $event['organizerActor']['name'] : $event['attributedTo']['name'] );
        add_post_meta($post_id, 'organizerURL',    (isset($event['organizerActor']['url'])) ? $event['organizerActor']['url'] : $event['attributedTo']['url'] );
        add_post_meta($post_id, 'street',          (isset($event['physicalAddress']['street'])) ? $event['physicalAddress']['street'] : "" );
        add_post_meta($post_id, 'place',           (isset($event['physicalAddress']['description'])) ? $event['physicalAddress']['description'] : "" );
        add_post_meta($post_id, 'city',            (isset($event['physicalAddress']['locality'])) ? $event['physicalAddress']['locality'] : "" );
        add_post_meta($post_id, 'postalCode',      (isset($event['physicalAddress']['postalCode'])) ? $event['physicalAddress']['postalCode'] : "" );
        add_post_meta($post_id, 'updatedAt',       $event['updatedAt']);
        add_post_meta($post_id, 'url',             $event['url']);
        add_post_meta($post_id, 'uuid',            $event['uuid']);
        add_post_meta($post_id, 'status',          $event['status']);
        if (isset($event['picture']['url'])) {
            $image = media_sideload_image( $event['picture']['url'], $post_id, $event['picture']['alt'], 'id');
            if (!is_wp_error($image) ) {
                set_post_thumbnail($post_id, $image);
            }
        }
        return $post_id;
    }

    /**
	 * Sets up a query to fetch an list of events from the set mobilizon server, not the details yet
	 *
	 * @since    1.0.0
	 * @access   private
     */
    private function mobilizon_set_up_event_list_query() {
        $group_name = get_option($this->plugin_name)['group_name'];
        $date_now = date("c");
        $query =   "query  {
                        group(preferredUsername: \"${group_name}\") {
                            organizedEvents(afterDatetime: \"{$date_now}\") {
                                elements {
                                    uuid,
                                    updatedAt,
                                }
                            }
                        }
                    }
                ";
        return $query;
    }


    /**
	 * Gets an event and all its details from an mobilizon server by the events uuid
	 *
	 * @since    1.0.0
	 * @access   private
     * @param    string     $uuid   https://de.wikipedia.org/wiki/Universally_Unique_Identifier
     */
    private function mobilizon_get_event($uuid) {
        $query =   "query  {
                        event(uuid: \"${uuid}\") {
                            uuid,
                            updatedAt,
                            title,
                            organizerActor {
                                name,
                                url
                            },
                            attributedTo {
                                name,
                                url
                            },
                            url,
                            beginsOn,
                            endsOn,
                            description,
                            onlineAddress,
                            status,
                            visibility,
                            picture {
								url,
                                alt
							},
                            tags {
                                slug,
                                title
                              },
                            physicalAddress {
                                street,
                                description,
                                locality,
                                postalCode
                            }
                        }
                    }";
        // Execute the event query to the mobilizon instance
        $response = $this->mobilizon_do_query(get_option($this->plugin_name)['instance_url'], $query);
        // Check if the HTTP-Query was successful
        if ( wp_remote_retrieve_response_code( $response ) != 200 ) {
            error_log ("Mobilizon did not respond");
            return false;
        }
        return json_decode( wp_remote_retrieve_body( $response ), true )['data']['event'];
    }


    /**
	 * Gets a list of all local posts of type mobilizon_event 
	 *
	 * @since    1.0.0
	 * @access   private
     * @return   array      keys: 'uuid', values: subarray with keys 'udpatedAt' and 'post_id'
     */
    private function get_local_mobilizon_events() {
        $args = array(
            'numberposts'	=> -1,
            'post_type'		=> 'mobilizon_event',
        );
        $the_query = new WP_Query( $args );
        $local_events = array();
        if( $the_query->have_posts() ):
            while( $the_query->have_posts() ) : $the_query->the_post();
                $local_events[get_post_meta( get_the_ID(), 'uuid', true )] = 
                    array(
                        'updatedAt' => get_post_meta( get_the_ID(), 'updatedAt', true),
                        'post_id'  => get_the_ID()
                    );
            endwhile;
        endif;
        wp_reset_postdata();
        return $local_events;
    }


    /**
	 * Gets a list of all remote events from the mobilizon server and group as set up in the options
	 *
	 * @since    1.0.0
	 * @access   private
     * @return   array      keys: 'uuid', values: updatedAt
     */
    private function get_remote_mobilizon_events() {
  
        // Get the query for all events (uuid, updatedAT)
        $query = $this->mobilizon_set_up_event_list_query();

        // Execute the event query to the mobilizon instance
        $response = $this->mobilizon_do_query(get_option($this->plugin_name)['instance_url'], $query);

        // Check if the HTTP-Query was successful
        if ( wp_remote_retrieve_response_code( $response ) != 200 ) {
            error_log ("Mobilizon did not respond");
            return;
        }
        
        // Extract the events as an array from the query's response body
        $body = json_decode(wp_remote_retrieve_body( $response ), true);
        $events_remote = $body['data']['group']['organizedEvents']['elements'];

        // Remote Event List by uuid as key:
        // the uuid serves as the array key, the updatedAt's are directly in the values

        return array_combine(array_column($events_remote,'uuid'), array_column($events_remote,'updatedAt') );

    }


    /**
	 * Compares list of remote event with list of local events and insert, update or delete events local ones
	 *
	 * @since    1.0.0
	 * @access   private    
     * @param    array      $local    local_events
     * @param    array      $remote   remote_events
     */
    private function update_mobilizon_events($local, $remote) {
        require_once(ABSPATH . 'wp-admin/includes/media.php');
        require_once(ABSPATH . 'wp-admin/includes/file.php');
        require_once(ABSPATH . 'wp-admin/includes/image.php');

        // Get events which uuids are not in remote but on local
        $to_delete = array_keys(array_diff_key($local,$remote));
        // Delete this events from wordpress
        foreach ($to_delete as $uuid) {
            $post_id = $local[$uuid]['post_id'];
            wp_delete_attachment( get_post_thumbnail_id($local[$uuid]['post_id']), $force_delete = true );
            if ( wp_delete_post($post_id, $force_delete = true) ) {
                unset($local[$uuid]);
            }
        }

        // Get events which uuids are in remote but not in local
        $to_add = array_keys(array_diff_key($remote,$local));
        // Inset this new events locally in wordress
        foreach ($to_add as $uuid) {
             // Fetch event details from the remote mobilizon server
            $event_details = $this->mobilizon_get_event($uuid);
            $post_id = $this->insert_or_update_mobilizon_event_as_post($event_details);
            if ( $post_id != 0  && !is_wp_error($post_id) ) {
                $local[$uuid] = array( 'post_id' => $post_id, 'updatedAt' => $remote[$uuid] );
            }
        }
        
        // Get events with same uuids but different updatedAt values
        $to_update = array_keys(array_diff_assoc(array_intersect_key($remote,$local),array_combine(array_keys($local), array_column($local, "updatedAt"))));
        // Update the local events
        foreach ($to_update as $uuid) {
            // Attachment will also be fetched again, so delete the old one!
            wp_delete_attachment( get_post_thumbnail_id($local[$uuid]['post_id']), $force_delete = true );
            // Fetch event details again from the remote mobilizon server
            $event_details = $this->mobilizon_get_event($uuid);
            // Insert the existing local post id, so that we update in the next step
            $event_details['post_id'] = $local[$uuid]['post_id'];
            $post_id =  $this->insert_or_update_mobilizon_event_as_post($event_details);
            if ( $post_id != 0  && !is_wp_error($post_id) ) {
                $local[$uuid] = array( 'post_id' => $post_id, 'updatedAt' => $remote[$uuid] );
            }
        }

        // Update the transient with the local mobilizon events
        delete_transient( 'mobilizon_mirror_cached_events_list');
        set_transient('mobilizon_mirror_cached_events_list', $local);

    }


    /**
	 * This is the main function which takes care of mirroring the mobilizon events on wordpress
	 *
	 * @since    1.0.0
	 * @access   private
     */
    public function refresh_mobilizon_events() {

        // Return if relevant options are not set
        if ( get_option($this->plugin_name)['group_name'] == '' || get_option($this->plugin_name)['instance_url'] == '' ) {
            error_log("this should not happen");
           return;
        }

        // Get list of local events
        if ( get_transient('mobilizon_mirror_cached_events_list') ) {
            $local_events = get_transient('mobilizon_mirror_cached_events_list');
        } else {
            $local_events = $this->get_local_mobilizon_events();
        }

        // Get list of remote events
        $remote_events = $this->get_remote_mobilizon_events();

        // Compare remote with local and insert, update or delete events
        $this->update_mobilizon_events($local_events, $remote_events);

    }   
    

    /**
	 * Add the custom interval how often the events are synced
	 *
	 * @since    1.0.0
	 * @access   private
     */
    public function mobilizon_mirror_add_minitly( $schedules ) {
        // Add a 'weekly' schedule to the existing set
        $schedules['mobilizon_mirror_refresh_interval'] = array(
            // TODO: Make this a user setting! 60sec are min, max could be up to an hour?
            'interval' => 120,
            'display' => __('Every minute', 'mobilizon-mirror')
        );
        return $schedules;

    }

}
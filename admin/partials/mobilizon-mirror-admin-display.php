<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://graz.social/@linos
 * @since      1.0.0
 *
 * @package    Mobilizon_Mirror
 * @subpackage Mobilizon_Mirror/admin/partials
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) die;
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="mobilizon-mirror-settings-wrapper">
    <div id="mobilizon-mirror-settings-title">
    <div id="mobilizon-mirror-logo" ></div>
    </div>

    <form method="post" name="<?php echo $this->plugin_name; ?>" action="options.php">
    <?php
        //Grab all options
        $options = get_option( $this->plugin_name );

        $instance_url = ( isset( $options['instance_url'] ) && ! empty( $options['instance_url'] ) ) ? esc_attr( $options['instance_url'] ) : '';
        $group_name = ( isset( $options['group_name'] ) && ! empty( $options['group_name'] ) ) ? esc_attr( $options['group_name'] ) : '';

        $event_archive_view =  ( isset( $options['event_archive_view'] ) && ! empty( $options['event_archive_view'] ) ) ? esc_attr( $options['event_archive_view'] ) : 'card';

        $event_single_view =  ( isset( $options['event_single_view'] ) && ! empty( $options['event_single_view'] ) ) ? esc_attr( $options['event_single_view'] ) : 'side';

        settings_fields($this->plugin_name);
        do_settings_sections($this->plugin_name);

    ?>

    <!-- Mobilizon Account Settings -->
    <fieldset>
        <legend><h2><?php esc_attr_e( 'Account Information', 'mobilizon-mirror' );?></h2></legend>
        <div class="components-base-control__wrapper">
            <!-- Mobilizon Instance URL -->
            <div class="components-base-control__field">
                <label><?php esc_attr_e( 'Mobilizon Instance (URL)', 'mobilizon-mirror' ); ?></label>
                <legend class="screen-reader-text">
                    <span><?php esc_attr_e( 'Mobilizon Instance (URL)', 'mobilizon-mirror' ); ?></span>
                </legend>
                <div class="input-wrapper">
                    <input type="text" class="textfield instance_url" list="mobilizon-mirror-instances" id="mobilizon-instance_url" name="<?php echo $this->plugin_name; ?>[instance_url]" value="<?php if( ! empty( $instance_url ) ) echo $instance_url; else echo ''; ?>"/>
                    <span id="mobilizon-instance_url-feedback-icon" class="form-control-feedback"></span>
                </div>
                <!-- TODO: Make sure that transient is reset, when it doesn not exist -->
                <?php echo get_transient(  'mobilizon_mirror_instance_datalist' ); ?>
            </div>

        <!-- Mobilizon Group Name -->
            <div class="components-base-control__field">
                <label><?php esc_attr_e( 'Federated Group Name', 'mobilizon-mirror' ); ?>   
                    <span class="btn label-info-button tooltip" data-tooltip="<?php esc_attr_e( 'Not the Display Name! &#10;The slug behind the @ in the url of your group page!', 'mobilizon-mirror')?>">&#8505;</span> 
                </label>
                <legend class="screen-reader-text">
                    <span><?php esc_attr_e( 'Federated Group Name', 'mobilizon-mirror' ); ?></span>
                </legend>
                <div class="input-wrapper">
                    <input type="text" class="textfield group_name"  list="mobilizon-group_names" id="mobilizon-group_name" name="<?php echo $this->plugin_name; ?>[group_name]" value="<?php if( ! empty( $group_name ) ) echo $group_name; else echo ''; ?>"/>
                    <span id="mobilizon-group_name-feedback-icon" class="form-control-feedback"></span>
                </div>
                <datafield id="mobilizon-group_names"></datafield>
            </div>
        </div>      
    </fieldset>
    
    
    <!-- Mobilizon Style Settings -->
    <fieldset>
        <legend><h2><?php esc_attr_e( 'Display Options', 'mobilizon-mirror' );?></h2></legend>
        <div class="components-base-control__wrapper">
            <!-- Mobilizon Archive Style -->
            <div class="components-base-control__field">
                <label><h3><?php esc_attr_e( 'Archive Style', 'mobilizon-mirror' ); ?></h3></label>
                <legend class="screen-reader-text">
                    <span><?php esc_attr_e( 'Archive Style', 'mobilizon-mirror' ); ?></span>
                </legend>
                <div class="radio-input-wrapper">
                    <input type="radio" id="<?php echo $this->plugin_name; ?>-event_archive_view" name="<?php echo $this->plugin_name; ?>[event_archive_view]" value="card" <?php if ($event_archive_view == 'card') { echo 'checked';}?> >
                    <label for="card"><?php esc_attr_e( 'Card View', 'mobilizon-mirror' ); ?></label><br>
                    <input type="radio" id="<?php echo $this->plugin_name; ?>-event_archive_view" name="<?php echo $this->plugin_name; ?>[event_archive_view]" value="list" <?php if ($event_archive_view == 'list') { echo 'checked';}?> >
                    <label for="list"><?php esc_attr_e( 'Simple List', 'mobilizon-mirror' ); ?></label><br>
                </div>
            </div>
        </div> 
        <div class="components-base-control__wrapper">
            <!-- Mobilizon Single Event Style -->
            <div class="components-base-control__field">
                <label><h3><?php esc_attr_e( 'Single Event: Position of Featured Image', 'mobilizon-mirror' ); ?></h3></label>
                <legend class="screen-reader-text">
                    <span><?php esc_attr_e( 'Single Event: Position of Featured Image', 'mobilizon-mirror' ); ?></span>
                </legend>
                <div class="radio-input-wrapper">
                    <input type="radio" id="<?php echo $this->plugin_name; ?>-event_single_view" name="<?php echo $this->plugin_name; ?>[event_single_view]" value="top" <?php if ($event_single_view == 'top') { echo 'checked';}?> >
                    <label for="top"><?php esc_attr_e( 'As Header', 'mobilizon-mirror' ); ?></label><br>
                    <input type="radio" id="<?php echo $this->plugin_name; ?>-event_single_view" name="<?php echo $this->plugin_name; ?>[event_single_view]" value="side" <?php if ($event_single_view == 'side') { echo 'checked';}?> >
                    <label for="side"><?php esc_attr_e( 'In Sidebar', 'mobilizon-mirror' ); ?></label><br>
                </div>
            </div>
        </div>      
    </fieldset>

  
    <?php submit_button( __( 'Save all changes', 'mobilizon-mirror' ), 'primary','submit', TRUE ); ?>
    </form>
</div>

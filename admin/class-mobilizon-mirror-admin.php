<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://graz.social/@linos
 * @since      1.0.0
 *
 * @package    Mobilizon_Mirror
 * @subpackage Mobilizon_Mirror/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Mobilizon_Mirror
 * @subpackage Mobilizon_Mirror/admin
 * @author     André Menrath <andre.menrath@posteo.de>
 */
class Mobilizon_Mirror_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Mobilizon_Mirror_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Mobilizon_Mirror_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/mobilizon-mirror-admin.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Mobilizon_Mirror_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Mobilizon_Mirror_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/mobilizon-mirror-admin.js', array( 'jquery' ), $this->version, false );

	}

		/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		/**
		 * Add a settings page for this plugin to the Settings menu.
		 *
		 * NOTE:  Alternative menu locations are available via WordPress administration menu functions.
		 *
		 *        Administration Menus: http://codex.wordpress.org/Administration_Menus
		 *
		 * add_options_page( $page_title, $menu_title, $capability, $menu_slug, $function);
		 *
		 * @link https://codex.wordpress.org/Function_Reference/add_options_page
		 *
		 * If you want to list plugin options page under a custom post type, then change 'plugin.php' to e.g. 'edit.php?post_type=your_custom_post_type'
		 */
		// Second-Last argument is none, we add the Mobilizon icon via css, because it's not available as an svg at the moment!
		// We add it where the custom post type menu would be as well
		add_menu_page(  'Mobilizon Settings', 'Mobilizon Events', 'manage_options', $this->plugin_name, array($this, 'display_plugin_setup_page'), 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgaWQ9Im1vYmlsaXpvbi1sb2dvIgogICB2aWV3Qm94PSIwIDAgNTQuOTk5OTk5IDU1LjAwMDAwMSIKICAgdmVyc2lvbj0iMS4xIgogICBzb2RpcG9kaTpkb2NuYW1lPSJtb2JpbGl6b24tbWlycm9yLnN2ZyIKICAgd2lkdGg9IjU1IgogICBoZWlnaHQ9IjU1IgogICBpbmtzY2FwZTp2ZXJzaW9uPSIxLjEgKGM0ZThmOWVkNzQsIDIwMjEtMDUtMjQpIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgogIDxkZWZzCiAgICAgaWQ9ImRlZnM4MSIgLz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgaWQ9Im5hbWVkdmlldzc5IgogICAgIHBhZ2Vjb2xvcj0iIzUwNTA1MCIKICAgICBib3JkZXJjb2xvcj0iI2VlZWVlZSIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZWNoZWNrZXJib2FyZD0iMCIKICAgICBzaG93Z3JpZD0iZmFsc2UiCiAgICAgaW5rc2NhcGU6em9vbT0iOC4wMzM3MDE1IgogICAgIGlua3NjYXBlOmN4PSIxNi41NzAwMDYiCiAgICAgaW5rc2NhcGU6Y3k9IjM0Ljk2MDk3MiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjE5MjAiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iMTAxMSIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iMCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9Imc3NCIKICAgICB1bml0cz0iaW4iCiAgICAgaW5rc2NhcGU6cm90YXRpb249Ii0xIiAvPgogIDxnCiAgICAgZGF0YS1uYW1lPSJDYWxxdWUgMiIKICAgICBpZD0iZzc2Ij4KICAgIDxnCiAgICAgICBkYXRhLW5hbWU9ImhlYWRlciIKICAgICAgIGlkPSJnNzQiPgogICAgICA8cGF0aAogICAgICAgICBmaWxsPSIjNDc0NDY3IgogICAgICAgICBkPSJtIDM4LjU3ODgyMywzMS40NzM4NzMgYyAwLDQuMjg4Njc5IC0xLjE3NzIwMSw3LjU4NTYwMSAtMy41MzE2LDkuODkwNzY3IC00LjI3NDYzLDMuNjY3MjQ2IC0xMC42OTczNjYsMy42NjcyNDYgLTE0Ljk3MTk5NSwwIC0yLjM1NDQsLTIuMjgzNzIyIC0zLjUzMTYsLTUuNTgwNjQ0IC0zLjUzMTYsLTkuODkwNzY3IDAsLTQuMzEwMTIyIDEuMTc3MiwtNy42MDk3MjUgMy41MzE2LC05Ljg5ODgwOCA0LjI3NDYyOSwtMy42NjcyNDYgMTAuNjk3MzY1LC0zLjY2NzI0NiAxNC45NzE5OTUsMCAyLjM1NDM5OSwyLjI4OTA4MyAzLjUzMTYsNS41ODg2ODYgMy41MzE2LDkuODk4ODA4IHogTSAyNy41NjEyMjUsMjMuMzY4MjcgYyAtMy4xMzM2NzMsMCAtNC43MDA1MDksMi43MDE4NjggLTQuNzAwNTA5LDguMTA1NjAzIDAsNS40MDM3MzcgMS41NjY4MzYsOC4xMDU2MDQgNC43MDA1MDksOC4xMDU2MDQgMy4xMzM2NzMsMCA0LjcwMDUxLC0yLjcwMTg2NyA0LjcwMDUxLC04LjEwNTYwNCAwLC01LjQwMzczNSAtMS41NjY4MzcsLTguMTA1NjAzIC00LjcwMDUxLC04LjEwNTYwMyB6IgogICAgICAgICBpZD0icGF0aDcwIgogICAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9InNjY3NjY3Nzc3NzcyIKICAgICAgICAgc3R5bGU9InN0cm9rZS13aWR0aDowLjgxNjQ3NiIgLz4KICAgICAgPHBhdGgKICAgICAgICAgZmlsbD0iI2ZmZDU5OSIKICAgICAgICAgZD0ibSAyNC42NjMwMzYsMTUuMzIxODYxIGMgLTAuMzM3OTM5LC0wLjc2MTUyNyAtMC41MDczOTcsLTEuNTgzMzU5IC0wLjQ5NzQwOCwtMi40MTIzODIgLTAuMDA5OSwtMC44MjkwMjMgMC4xNTk0NjksLTEuNjUwODU0IDAuNDk3NDA4LC0yLjQxMjM4MSAwLjk5NTIyNCwtMC40Nzk0ODkgMi4wOTc2OCwtMC43MTE1OTg1IDMuMjA4Mjg1LC0wLjY3NTQ2NzggMS4wNjQwOTgsLTAuMDQ5NDc5IDIuMTIxODk1LDAuMTg0MDkyOCAzLjA1OTA2MiwwLjY3NTQ2NzggMC4zMzc5NCwwLjc2MTUyNyAwLjUwNzM5OCwxLjU4MzM1OCAwLjQ5NzQwOSwyLjQxMjM4MSAwLjAxMDAzLDAuODI5MDIzIC0wLjE1OTQ2OSwxLjY1MDg1NSAtMC40OTc0MDksMi40MTIzODIgLTAuOTg3MTY1LDAuNTAxNDAyIC0yLjA5NTY0OCwwLjczNDc4MSAtMy4yMDgyODQsMC42NzU0NjcgLTEuMDY2MjI4LDAuMDc0NjIgLTIuMTMxMTk3LC0wLjE2MDUzNiAtMy4wNTkwNjMsLTAuNjc1NDY3IHoiCiAgICAgICAgIGlkPSJwYXRoNzIiCiAgICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0iY2NjY2NjY2NjIgogICAgICAgICBzdHlsZT0ic3Ryb2tlLXdpZHRoOjAuODE2NDc2IiAvPgogICAgPC9nPgogIDwvZz4KPC9zdmc+Cg==', 21  );
		add_submenu_page( $this->plugin_name, esc_html__('Frequently Asked Questions', $this->plugin_name) , 'FAQ', 'manage_options', 'mobilizon_mirror_faq', array($this, 'display_plugin_faq_page'), 1 );
		global $submenu;
		$submenu[$this->plugin_name][0][0] = esc_html__('Settings', $this->plugin_name);
	
	}


	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */
	public function add_action_links( $links ) {

		/**
		 * Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
		 * The "plugins.php" must match with the previously added add_submenu_page first option.
		 * For custom post type you have to change 'plugins.php?page=' to 'edit.php?post_type=your_custom_post_type&page='
		 */
		$settings_link = array( '<a href="' . admin_url( 'plugins.php?page=' . $this->plugin_name ) . '">' . __( 'Settings', $this->plugin_name ) . '</a>', );

		// -- OR --

		//$settings_link = array( '<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_name ) . '">' . __( 'Settings', $this->plugin_name ) . '</a>', );

		return array_merge(  $settings_link, $links );

	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_setup_page() {

		include_once( 'partials/' . $this->plugin_name . '-admin-display.php' );

	}

		/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_faq_page() {

		include_once( 'partials/' . $this->plugin_name . '-faq-display.php' );

	}




	/**
	 * Validate if the url is a valid mobilizon instance using webfinger
	 * https://webfinger.net/
	 * @param  string url 
	 * @return bool 
	 */
	public function is_mobilizon_instance($url) {
	
		if (wp_http_validate_url($url)) {
			$response = wp_remote_get( $url . '/.well-known/nodeinfo/2.1');
			if (!is_wp_error($response)) {
				if ( isset($response['body']) ) {
					$webfinger = json_decode($response['body'], true);
					if( isset($webfinger['software']['name']) ){
						if ($webfinger['software']['name'] === "Mobilizon") {
							return true;
						}
					}
				}
			}
		}
		return false;
	}


	/**
	 * Validate if a group exists on a mobilizon server
	 * Only 
	 * @param  string url 
	 * @return bool 
	 */
	public function is_mobilizon_group($url, $group_name) {

		// Get API-endpoint from Instance URL
		$url = rtrim($url, '/');
		$url_array = array($url, "api");
		$endpoint = implode('/', $url_array);

		// Define query
		$query = "query {
						group(preferredUsername: \"${group_name}\") {
							type
						}
					}
					";

		// Define default GraphQL headers
		$headers = ['Content-Type: application/json', 'User-Agent: Minimal GraphQL client'];
		$body = array ('query' => $query);
		$args = array(
			'body'        => $body,
			'headers' 	  => $headers,
		);

		// Send HTTP-Query and return the response
		$response = wp_remote_post($endpoint, $args);
		if (!is_wp_error($response)) {
			if ( isset($response['body']) ) {
				$body = json_decode($response['body'], true);
				if( isset($body['data']['group']['type']) ){
					if ($body['data']['group']['type'] === "GROUP") {
						return true;
					}
				}
			}
		}

		return false;


	}

	public function add_http_if_not_present($url) {
		// Add https protokoll if not present
		if ( $ret = parse_url($url) && !isset($ret["scheme"]) )  {
			$host = parse_url($url, PHP_URL_HOST);
			if ($host != null) {
				$url = "https://{$host}";
			} else {
				$url = "https://{$url}";
			}
		}
		return $url;
	}

	/**
	 * Validate fields from admin area plugin settings form ('exopite-lazy-load-xt-admin-display.php')
	 * @param  mixed $input as field form settings form
	 * @return mixed as validated fields
	 */
	public function validate($input) {

		$options = get_option( $this->plugin_name );

		$old_group_name = $options['group_name'];
		$old_instance_url =  $options['instance_url'];

		// Instance URL 
		// make input save:
		$instance_url_input = ( isset( $input['instance_url'] ) && ! empty( $input['instance_url'] ) ) ? esc_attr( $input['instance_url'] ) : '';

		// Add https protokoll if not present
		$instance_url_input = $this->add_http_if_not_present($instance_url_input);

		// Set option if the input was a valid mobilizon url, other clean it
		$options['instance_url'] = ( $this->is_mobilizon_instance($instance_url_input) ) ? $instance_url_input : '';

		// Only check, if valid instance url is already set
		if ( $options['instance_url'] != '' ) {
			// Group Name
			$group_name_input = ( isset( $input['group_name'] ) && ! empty( $input['group_name'] ) ) ? esc_attr( $input['group_name'] ) : '';

			// Mobilizon's ferderated group name only allows small letters and underscores
			$group_name_input = strtolower(str_replace(" ", "_", $group_name_input));

			// Set option if the input was a group that exists
			$options['group_name'] = ( $this->is_mobilizon_group($options['instance_url'], $group_name_input) ) ? $group_name_input : '';
		} else {
			$options['group_name'] = '';
		}

		// Set the "easy" options
		$options['event_archive_view'] = ( isset( $input['event_archive_view'] ) && ! empty( $input['event_archive_view'] ) ) ? esc_attr( $input['event_archive_view'] ) : 'card';
		$options['event_single_view'] =  ( isset( $input['event_single_view'] ) && ! empty( $input['event_single_view'] ) ) ? esc_attr( $input['event_single_view'] ) : 'top';


		// Check if the settings for instance or group have changed
		if ( $old_instance_url != $options['instance_url'] && $old_group_name != $options['group_name'] ) {
			// If we have changed to a valid instance and group delte (old) cronjob if its there
			$timestamp = wp_next_scheduled( 'mobilizon_mirror_cron_refresh_events' );
			if ($timestamp) {
				wp_unschedule_event( $timestamp, 'mobilizon_mirror_cron_refresh_events');
				error_log("wp_event go unscheduled at timestamp: " . $timestamp);
			}
			// and create a new cron job for fetching the events if instance and group are valid!
			if (  $options['instance_url'] != '' && $options['group_name'] != '') {
				//  But of course only, if the cronjob does not exist yet, just ut be 100% sure...
				if ( ! wp_next_scheduled( 'mobilizon_mirror_cron_refresh_events' ) ) {
					wp_schedule_event( time(), 'mobilizon_mirror_refresh_interval', 'mobilizon_mirror_cron_refresh_events' );
				}
			}
		}

		return $options;

	}

	public function options_update() {

		register_setting( $this->plugin_name, $this->plugin_name, array(
		'sanitize_callback' => array( $this, 'validate' ),
		) );
		
	}


	public function is_mobilizon_group_callback() {

		$instance_url =  esc_attr( $_POST['instance_url'] );
		$group_name = esc_attr( $_POST['group_name'] );

        $instance_url = $this->add_http_if_not_present($instance_url);
		
		echo $this->is_mobilizon_group($instance_url, $group_name);

    	die();

	}


	public function is_mobilizon_instance_callback() {

		$instance_url = esc_attr( $_POST['instance_url'] ) ;
		
        $instance_url = $this->add_http_if_not_present($instance_url);
		
		echo $this->is_mobilizon_instance($instance_url);
		
    	die();

	}
	

}



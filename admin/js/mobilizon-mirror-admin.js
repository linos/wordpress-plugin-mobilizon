(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

})( jQuery );

// Still a mess, ask for help to a proper JavaScript Developer
jQuery(document).ready( function($) {
	var url = document.getElementById("mobilizon-instance_url");
	var urlIcon = document.getElementById("mobilizon-instance_url-feedback-icon");
	var group =  document.getElementById("mobilizon-group_name")
	var groupIcon = document.getElementById("mobilizon-group_name-feedback-icon");

	function checkInstanceField() {
		resetGroupField();
		if ( url.value.split(".").length < 2)  {
			return;
		}
		if ( url.value.length < 5) {
			return;
		}
		if (url.value.split("/@").length == 2) {
			let actor = url.value.split("/@");
			url.value = actor[0];
			group.value = actor[1];
		}
		let data = {
			action: 'is_mobilizon_instance',
			instance_url: url.value
		};
		jQuery.post(ajaxurl, data, function(response) {
			if (response) {
				url.classList.add("input-valid");
				url.classList.remove("input-warning");
				urlIcon.classList.add("icon-valid");
				urlIcon.classList.remove("icon-warning");
				group.style.display = "block"; 
				checkGroupField();
			} else {
				url.classList.add("input-warning");
				url.classList.remove("input-valid");
				urlIcon.classList.add("icon-warning");
				urlIcon.classList.remove("icon-valid");
			}
		});
	}

	function checkGroupField() {
		group.value = group.value.toLowerCase().replace('-', '');
		if ( group.value == '' ) {
			return;
		}
		let data = {
			action: 'is_mobilizon_group',
			instance_url: url.value,
			group_name: group.value
		};
		jQuery.post(ajaxurl, data, function(response) {
			//document.getElementById("mobilizon-group_names").innerHTML = response;
			if (response) {
				group.classList.add("input-valid");
				group.classList.remove("input-warning");
				groupIcon.classList.add("icon-valid");
				groupIcon.classList.remove("icon-warning");
				url.classList.add("input-valid");
				url.classList.remove("input-warning");
				urlIcon.classList.add("icon-valid");
				urlIcon.classList.remove("icon-warning");
			} else {
				group.classList.add("input-warning");
				group.classList.remove("input-valid");
				groupIcon.classList.add("icon-warning");
				groupIcon.classList.remove("icon-valid");
			}
		});
	}

	function resetGroupField() {
		if (group.value != '') {
			group.classList.remove("input-valid");
			groupIcon.classList.remove("icon-valid");
		}
	}


	$("#mobilizon-instance_url").on("paste",function () {
		setTimeout(function(){ checkInstanceField(); },  100);
	});
	$("#mobilizon-instance_url").on("keyup", checkInstanceField);

	$("#mobilizon-group_name").on("keyup", checkGroupField);
	if ( url.value != '' ) {
		checkInstanceField();
	}
	if ( group.value != '' ) {
		checkGroupField();
	}
	
});

=== Plugin Name ===
Contributors: andremenrath
Donate link: https://liberapay.com/graz.social/
Tags: mobilizon, events, calendar
Requires at least: 5.8
Tested up to: 5.8
Stable tag: 5.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Mirror the events of your Mobilizon group on your WordPress site.


== Description ==

This plugin intergrates mobilizon events via creating it's own read only custom post type inside WordPress. Events get synced every two minutes.


== Features ==

*   It is designed to integrate well into your theme, though you may override the archive and single pages for the mobilizon_event post type in your theme.
*   Archive Page: Beautiful Card-View and very simple List-View
*   Single Event Pages: Choose Position of Featured Image
*   Recognizes if an event has been updated or deleted on Mobilizon


== Installation and Setup ==

1. Activate the plugin through the 'Plugins' menu in WordPress
2. Go to the settings page (you can also find it in the admin-menu as a submenu of the plugins-tab)


== Screenshots ==

1. Archive page with card style
2. Archive page with simple list style
3. Event with image as header
4. Event with image in sidebar


== Changelog ==

= 1.0.0 =
* Initial Release


== Upgrade Notice ==


== Arbitrary section ==




